//
//  ViewController.swift
//  HideNavigationBar
//
//  Created by iulian david on 7/11/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var btnFind: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        btnFind.layer.cornerRadius = btnFind.bounds.width / 2
        btnFind.clipsToBounds = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

